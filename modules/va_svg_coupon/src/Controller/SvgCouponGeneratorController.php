<?php

namespace Drupal\va_svg_coupon\Controller;

use Drupal\Core\Url;
use Drupal\block\Entity\Block;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\Color;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Com\Tecnick\Barcode\Barcode as BarcodeGenerator;

use Drupal\vap_coupon_page\Library\CouponCode;
use Drupal\va_svg_generator\Library\SvgGenerator;

/**
 * Provides route responses for the SvgGenerator module.
 */
class SvgCouponGeneratorController extends ControllerBase {
  /**
   * Returns a coupon page.
   *
   * @return array
   *   A renderable array.
   */
  public function office_depot_flyer() {    
    if (\Drupal::currentUser()->isAnonymous()) {
      $destination  = \Drupal::service('path.alias_manager')->getAliasByPath(\Drupal::service('path.current')->getPath());
      $url          = Url::fromRoute('user.login', [], ['query' => ['destination' => $destination]])->toString();
      $response     =  new RedirectResponse($url);
      $response->send();
      exit(0);
    }
    
    $render = new Response();
    $render->setContent(SvgCouponGeneratorController::office_depot_builder());

    return $render;
  }


  /**
   * Office Depot Coupon Page
   */
  private function office_depot_builder() {
    $config = $this->config('va_svg_coupon.settings');
    if (empty($config)) {
      throw new AccessDeniedHttpException();
    }
    
    $coupon_config = $this->config('coupon_code.settings');
    $partner       = (int) CouponCode::get_partner_id_from_name("Office Depot");
    $options       = array(
      'expires'  => 'onRefresh',
      'settings' => array(
        'member_id'    => TRUE,
        'set_expires'  => $coupon_config->get('partner_set_to_expire'),
        'user_expires' => $coupon_config->get('partner_expire_period'),
      ),
    );
    $drupal_user = User::load(\Drupal::currentUser()->id());
    $memberid    = $drupal_user->get('field_memberid')->value;
    $barcode = CouponCode::retrieve_member_code($partner, $memberid, $options);
    
    $bcg        = new BarcodeGenerator();
    $bcgImg     = $bcg->getBarcodeObj(
      'C128',           // code type ($bcg->getTypes())
      $barcode->code,   // coupon code
      250,              // width
      75,               // height
      'black',          // color (css predefined colors)
      array(0, 0, 0, 0) // padding (top, right, bottom, left)
    )->getSvgCode();    
    $date       = date('m-d-Y', $barcode->expires); //TODO: uhhhh... what now?

    $svg        = new SvgGenerator($config->get('max_width'), $config->get('max_height'));

    $rect = array(
      'tag' => 'rect',
      'x' => '0',
      'y' => '0',
      'fill' => 'rgb('.implode(',', Color::hexToRgb("#FFFFFF")).')',
      'stroke' => 'rgb('.implode(',', Color::hexToRgb($config->get('border_color'))).')',
      'stroke-dasharray' => '20,10',
      'stroke-width' => '10',
      'width' => '100%',
      'height' => '100%',
    );
    $svg->add_field('bkg', 'bkg', $rect);


    $opts = array (
      'tag' => 'image',
      'x' => '2.5%',
      'y' => '3%',
      'xlink:href' => file_create_url($config->get('header_image_path')),
      'width' => '96.5%',
      'height' => '25%',
    );
    $svg->add_field('header', 'header', $opts);


    $rect = array(
      'tag' => 'rect',
      'x' => '2.5%',
      'y' => '29.5%',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('header_bg_color'))).')',
      'width' => '95%',
      'height' => '21%',
    );
    $svg->add_field('discountheaderbkg', 'discountheaderbkg', $rect);

    $opts = array (
      'x' => '50%',
      'y' => '43%',
      'class' => 'header',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('header_font_color'))).')',
      'font-size' => '1100%',
      'font-family' => 'Poppins',
      'font-weight' => 'bold',
      'text-anchor' => 'middle',
    );
    $svg->add_child_tag('discountheader_text', 'percentage', 'tspan', $opts, $config->get('header_text'));

    $opts = array (
      'x' => '50%',
      'y' => '46.2%',
      'class' => 'header',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('header_font_color'))).')',
      'font-size' => '220%',
      'font-family' => 'Poppins',
      'font-weight' => 'bold',
      'text-anchor' => 'middle',
    );
    $svg->add_child_tag('discountheader_text', 'header', 'tspan', $opts, $config->get('header_subtext'));


    $opts = array (
      'tag' => 'text',
      'x' => '50%',
      'y' => '53.8%',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('sub_header_font_color'))).')',
      'font-size' => '200%',
      'font-family' => 'Open Sans',
      'font-weight' => 'bold',
      'text-anchor' => 'middle',
    );
    $svg->add_element('ctaheader', 'ctaheader', $opts, $config->get('sub_header_text'));


    $opts = array (
      'tag' => 'text',
      'x' => '50%',
      'y' => '60.7%',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('expires_font_color'))).')',
      'font-family' => 'Open Sans',
      'font-weight' => 'bold',
      'text-anchor' => 'middle',
      'font-size' => '150%',
    );
    $svg->add_element('expires', 'expires', $opts, $config->get('expires_header_text') . ' ' . $date);

    /* Library\BarcodeGenerator */
    $opts = array (
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('barcode_font_color'))).')',
      'tag' => 'text',
      'x' => '49.9%',
      'y' => '62.5%',
      'text-anchor' => 'middle',
    );
    $svg->add_element('barcode', 'barcode', $opts, $barcode->code);

    $opts = array (
      'tag' => 'foreignobject',
      'x' => '37.5%',
      'y' => '63%',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('barcode_font_color'))).')',
      'width' => '250px',
      'height' => '75px',
    );
    $svg->add_element('barcodeimage', 'barcodeimage', $opts, (empty($barcode->error)) ? $bcgImg : 'Error finding code.');
    /* end Library\BarcodeGenerator */


    $rect = array(
      'tag' => 'rect',
      'x' => '39.75%',
      'y' => '69.6%',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('cta_bg_color'))).')',
      'stroke' => 'rgb('.implode(',', Color::hexToRgb($config->get('cta_bg_color'))).')',
      'stroke-width' => '1',
      'rx' => '5',
      'ry' => '5',
      'width' => '20.5%',
      'height' => '2%',
    );
    $svg->add_field('codebkg', 'codebkg', $rect);


    $opts = array (
      'tag' => 'polygon',
      'generate' => '_generate_od_arrow_points',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('cta_bg_color'))).')',
    );
    $svg->add_field('od_arrow', 'od_arrow', $opts);


    $opts = array (
      'tag' => 'text',
      'x' => '50%',
      'y' => '71%',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('cta_font_color'))).')',
      'text-anchor' => 'middle',
      'font-family' => 'Open Sans',
      'font-weight' => 'bold',
    );
    $svg->add_element('codectatext', 'codectatext', $opts, $config->get('cta_text'));


    $opts = array (
      'tag' => 'text',
      'x' => '49.5%',
      'y' => '74%',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('barcode_font_color'))).')',
      'text-anchor' => 'middle',
      'font-family' => 'Open Sans',
      'font-weight' => 'bold',
      'font-size' => '70%',
    );
    $svg->add_element('codetext', 'codetext', $opts, $barcode->code);


    $opts = array (
      'tag' => 'foreignobject',
      'x' => '2.5%',
      'y' => '75.5%',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('legal_font_color'))).')',
      'width' => '95%',
      'height' => '12%',
      'font-size' => '95%',
    );
    $svg->add_element('legal', 'legal', $opts, $config->get('legal_text'));


    $opts = array (
      'tag' => 'text',
      'x' => '2.5%',
      'y' => '91%',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('legal_cta_font_color'))).')',
      'font-family' => 'Open Sans',
      'font-weight' => 'bold',
      'font-size' => '110%',
    );
    $svg->add_element('legalexpire', 'legalexpire', $opts, $config->get('legal_cta_text'));


    $rect = array(
      'tag' => 'rect',
      'x' => '2.5%',
      'y' => '91.5%',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('print_cta_bg_color'))).')',
      'width' => '95%',
      'height' => '6%',
    );
    $svg->add_field('legalheaderbkg', 'legalheaderbkg', $rect);

    $opts = array (
      'tag' => 'text',
      'x' => '50%',
      'y' => '95%',
      'fill' => 'rgb('.implode(',', Color::hexToRgb($config->get('print_cta_font_color'))).')',
      'font-size' => '185%',
      'font-family' => 'Poppins',
      'text-anchor' => 'middle',
    );
    $svg->add_element('legalheader', 'legalheader', $opts, $config->get('print_cta_text'));

    /* Weird hack to make the legal text as responsive as the rest */
    $svg->add_style('@media only screen and (min-width:300px)', array('#legal' => array('font-size' => '100%')));

    return $svg->draw();
  }

}
