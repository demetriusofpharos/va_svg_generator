<?php

namespace Drupal\va_svg_coupon\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Provides a Form to generate a coupon from
 *
 * @Form (
 *    id= = 'svg_coupon_form'
 *    admin_label = @Translation("SVG Coupon Form")
 * )
 */

class SvgCouponForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "svg_coupon_admin_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('va_svg_coupon.settings');

    $form = array();

    $form['coupon_svg']['max_width'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Width'),
      '#default_value'  => ($config->get('max_width')) ? $config->get('max_width') : '1000',
    );

    $form['coupon_svg']['max_height'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Height'),
      '#default_value'  => ($config->get('max_height')) ? $config->get('max_height') : '1250',
    );


    $form['coupon_svg']['border_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Border Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('border_color')) ? $config->get('border_color') : '#CCCCCC',
    );


    $form['coupon_svg']['header_image_path'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Path to header image'),
      '#default_value'  => ($config->get('header_image_path')) ? $config->get('header_image_path') : '',
    );

    $form['coupon_svg']['header_image_upload'] = array(
      '#type'               => 'managed_file',
      '#upload_location'    => 'public://upload/coupon/',
      '#upload_validators'  => array(
        'file_validate_is_image'    => array(),
        'file_validate_extensions'  => array('png gif jpg jpeg apng svg'),
      ),
      '#title'            => t('Upload header image'),
      '#maxlength'        => 150,
      '#description'      => t("<small>Use this field to upload the header image.</small>")
    );


    $form['coupon_svg']['header_bg_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Header Background Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('header_bg_color')) ? $config->get('header_bg_color') : '#FF0000',
    );

    $form['coupon_svg']['header_font_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Header Font Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('header_font_color')) ? $config->get('header_font_color') : '#FFFFFF',
    );

    $form['coupon_svg']['header_text'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Header Text'),
      '#description'    => '<small>Text will be rendered exactly as entered</small>',
      '#default_value'  => ($config->get('header_text')) ? $config->get('header_text') : '',
    );

    $form['coupon_svg']['header_subtext'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Header Sub Text'),
      '#description'    => '<small>Text will be rendered exactly as entered</small>',
      '#default_value'  => ($config->get('header_subtext')) ? $config->get('header_subtext') : '',
    );


    $form['coupon_svg']['sub_header_font_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Sub header Font Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('sub_header_font_color')) ? $config->get('sub_header_font_color') : '#CCCCCC',
    );

    $form['coupon_svg']['sub_header_text'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Sub header Text'),
      '#description'    => '<small>Text will be rendered exactly as entered</small>',
      '#default_value'  => ($config->get('sub_header_text')) ? $config->get('sub_header_text') : '',
    );


    $form['coupon_svg']['expires_font_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Expires Font Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('expires_font_color')) ? $config->get('expires_font_color') : '#000000',
    );

    $form['coupon_svg']['expires_header_text'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Expires'),
      '#description'    => '<small>Text will be rendered exactly as entered</small>',
      '#default_value'  => ($config->get('expires_header_text')) ? $config->get('expires_header_text') : 'Expires',
    );
    

    $form['coupon_svg']['barcode_font_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Barcode Font Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('barcode_font_color')) ? $config->get('barcode_font_color') : '#000000',
    );


    $form['coupon_svg']['cta_bg_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'CTA background Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('cta_bg_color')) ? $config->get('cta_bg_color') : '#FF0000',
    );

    $form['coupon_svg']['cta_font_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'CTA Font Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('cta_font_color')) ? $config->get('cta_font_color') : '#FFFFFF',
    );

    $form['coupon_svg']['cta_text'] = array(
      '#type'           => 'textfield',
      '#title'          => t('Expires'),
      '#description'    => '<small>Text will be rendered exactly as entered</small>',
      '#default_value'  => ($config->get('cta_text')) ? $config->get('cta_text') : 'USE THIS CODE ONLINE',
    );


    $form['coupon_svg']['legal_font_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Legal Font Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('legal_font_color')) ? $config->get('legal_font_color') : '#000000',
    );

    $form['coupon_svg']['legal_text'] = array(
      '#type'            => 'text_format',
      '#format'          => 'full_html',
      '#title'           => t('Legal text'),
      '#description'    => '<small>Text will be rendered exactly as entered (please use <a href="http://www.htmlhelp.com/reference/html40/entities/">entities</a> where appropriate</small>)',
      '#default_value'   => ($config->get('legal_text')) ? $config->get('legal_text') : '',
    );


    $form['coupon_svg']['legal_cta_font_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Legal CTA Font Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('legal_cta_font_color')) ? $config->get('legal_cta_font_color') : '#000000',
    );

    $form['coupon_svg']['legal_cta_text'] = array(
      '#type'            => 'textfield',
      '#title'           => t('Legal CTA text'),
      '#description'    => '<small>Text will be rendered exactly as entered</small>',
      '#default_value'   => ($config->get('legal_cta_text')) ? $config->get('legal_cta_text') : '',
    );


    $form['coupon_svg']['print_cta_bg_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Print CTA background Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('print_cta_bg_color')) ? $config->get('print_cta_bg_color') : '#000000',
    );

    $form['coupon_svg']['print_cta_font_color'] = array(
      '#type'           => 'textfield',
      '#title'          => 'Print CTA Font Color',
      '#description'    => '<small>Hex codes only</small>',
      '#default_value'  => ($config->get('print_cta_font_color')) ? $config->get('print_cta_font_color') : '#FFFFFF',
    );

    $form['coupon_svg']['print_cta_text'] = array(
      '#type'            => 'textfield',
      '#title'           => t('Print CTA text'),
      '#description'    => '<small>Text will be rendered exactly as entered</small>',
      '#default_value'   => ($config->get('print_cta_text')) ? $config->get('print_cta_text') : '',
    );

    return parent::buildForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  /*public function validateForm(array &$form, FormStateInterface $form_state) {
   
  }*/

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    
    if (isset($values['header_image_upload'][0]) && 
              $values['header_image_upload'][0] != "") {
      $file = File::load($values['header_image_upload'][0]);
      $file->status = FILE_STATUS_PERMANENT;
      $file->save();
      $values['header_image_path'] = $file->getFileUri();
    }

    unset($values['header_image_upload']);
    unset($values['form_build_id']);
    unset($values['form_token']);
    unset($values['form_id']);
    unset($values['op']);
    unset($values['submit']);
    foreach ($values as $k => $v) {
      if (is_array($v)) {
        $v = $v['value'];
      }
      $this->config('va_svg_coupon.settings')->set($k, $v)->save();
    }
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'va_svg_coupon.settings',
    ];
  }
}
