<?php

namespace Drupal\va_svg_digitalcard\Controller;

use Drupal\Component\Utility\Color;
use Drupal\Core\Controller\ControllerBase;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\Response;

use Drupal\va_svg_generator\Library\SvgGenerator;

/**
 * Provides route responses for the SvgGenerator module.
 */
class SvgDigitalCardGeneratorController extends ControllerBase {

  /**
   * The user object.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
    * override config setting
    */
   protected $width;
   
   /**
    * override config setting
    */
   protected $height;

  /**
   * Returns a digital card page.
   *
   * @return array
   *   A renderable array.
   */
  public function digitalCardPage($uid) {
    $this->user = User::load($uid);

    $render = new Response();
    $render->setContent(SvgDigitalCardGeneratorController::digitalCardBuilder());

    return $render;
  }

  /**
   * Digital Rewards Card form element.
   */
  public function digitalCardFormElement($user) {
    $this->user = $user;
    $this->width  = 600;
    $this->height = 400;
    return $this->digitalCardBuilder();
  }

  /**
   * Digital Rewards Card.
   */
  private function digitalCardBuilder() {
    $branch_tree = $this->entityTypeManager()->getStorage('taxonomy_term')
      ->loadTree('military_branches');
    $branches = [];
    foreach ($branch_tree as $branch) {
      $branches[$branch->tid] = strtoupper($branch->name);
    }

    if (!empty($this->user->get('field_expiry_date')->value)) {
      $expiration = date('Y-m-d', strtotime($this->user->get('field_expiry_date')->value));
    }

    $vetinfo = array(
      'vetname'    => $this->user->get('field_first_name')->value . ' ' . $this->user->get('field_last_name')->value,
      'vetid'      => $this->user->get('field_memberid')->value,
      'vetexpires' => (!empty($expiration)) ? $expiration : '',
    );

    if (!empty($this->user->get('field_military_branch')->target_id)
        && in_array($this->user->get('field_military_branch')->target_id, array_keys($branches))) {
      $vetinfo['vetbranch'] = $branches[$this->user->get('field_military_branch')->target_id];
    }

    $config = $this->config('va_svg_digitalcard.settings');
    $width  = ($this->width) ? $this->width : $config->get('width') ;
    $height = ($this->height) ? $this->height : $config->get('height') ;
    $svg    = new SvgGenerator($width, $height);

    $svg->add_field('bkg', 'bkg', [
      'tag'    => 'rect',
      'x'      => '0',
      'y'      => '0',
      'fill'   => 'rgb(' . implode(',', Color::hexToRgb('#FFFFFF')) . ')',
      'width'  => '100%',
      'height' => '100%',
    ]);

    $svg->add_field('backgroundimage', 'background', [
      'tag'        => 'image',
      'x'          => '0',
      'y'          => '0',
      'xlink:href' => file_create_url($config->get('background_image_path')),
      'width'      => '100%',
      'height'     => '100%',
    ]);

    $svg->add_element('vetname', 'vetname', [
      'tag'         => 'text',
      'x'           => '3.5%',
      'y'           => '50%',
      'font-size'   => '150%',
      'font-family' => 'Open Sans',
      'font-weight' => 'bold',
      'text-anchor' => 'left',
    ], $vetinfo['vetname']);

    if (!empty($vetinfo['vetbranch'])) {
      $svg->add_element('branch', 'branch', [
        'tag' => 'text',
        'x' => '3.5%',
        'y' => '58%',
        'font-size' => '145%',
        'font-family' => 'Open Sans',
        'font-weight' => 'bold',
        'text-anchor' => 'left',
      ], $vetinfo['vetbranch']);
    }

    $svg->add_element('vetid', 'vetid', [
      'tag' => 'text',
      'x' => '3.5%',
      'y' => '88%',
      'font-size' => '90%',
      'font-family' => 'Open Sans',
      'text-anchor' => 'left',
    ], 'MEMBER ID: ' . $vetinfo['vetid']);

    $svg->add_element('expires', 'expires', [
      'tag' => 'text',
      'x' => '3.5%',
      'y' => '93%',
      'font-size' => '80%',
      'font-family' => 'Open Sans',
      'text-anchor' => 'left',
    ], 'VALID UNTIL: ' . $vetinfo['vetexpires']);

    return $svg->draw();
  }

}
