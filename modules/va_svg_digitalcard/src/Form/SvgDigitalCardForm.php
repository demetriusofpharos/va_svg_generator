<?php

namespace Drupal\va_svg_digitalcard\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Provides a Form to generate a coupon from.
 *
 * @Form (
 *   id= = 'svg_digital_card_form'
 *   admin_label = @Translation("SVG Digital Card Form")
 * )
 */
class SvgDigitalCardForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return "svg_digital_card_admin_form";
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('va_svg_digitalcard.settings');

    $form['digital_card']['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => ($config->get('width')) ? $config->get('width') : 1000,
      '#min' => 0,
    ];

    $form['digital_card']['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => ($config->get('height')) ? $config->get('height') : 635,
      '#min' => 0,
    ];

    $form['digital_card']['background_image_path'] = [
      '#type' => 'textfield',
      '#title' => t('Path to background image'),
      '#default_value' => $config->get('background_image_path') ?: '',
    ];

    $form['digital_card']['background_image_upload'] = [
      '#type' => 'managed_file',
      '#upload_location' => 'public://upload/digital_card/',
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_extensions' => ['png gif jpg jpeg apng svg'],
      ],
      '#title' => t('Upload background image'),
      '#description' => $this->t('<small>Use this field to upload the background image.</small>'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    if (!empty($values['background_image_upload'][0])) {
      $file = File::load($values['background_image_upload'][0]);
      $file->status = FILE_STATUS_PERMANENT;
      $file->save();
      $values['background_image_path'] = $file->getFileUri();
    }

    $config = $this->config('va_svg_digitalcard.settings');
    foreach (['width', 'height', 'background_image_path'] as $key) {
      $config->set($key, $values[$key]);
    }
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'va_svg_digitalcard.settings',
    ];
  }

}
