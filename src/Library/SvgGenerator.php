<?php

namespace Drupal\va_svg_generator\Library;


use Drupal\file\Entity\File;

/**
 * Generates SVGs.
 *
 */
class SvgGenerator {
  public $width;
  public $height;

  protected $fields;
  protected $style;


  /* PUBLIC FUNCTIONS */
  /**
   *
   */
  public function __construct($width, $height) {
    $this->width    = $width;
    $this->height   = $height;

    $this->fields   = new \stdClass();
    $this->style    = new \stdClass();
  }


  /**
   *
   */
  public function draw() {
    $xml  = '';
    /*
    $xml .= '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';
    $xml .= '<svg width="' . $this->width . '" height="' . $this->height . '" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink= "http://www.w3.org/1999/xlink">';
    */
    $xml .= $this->build_xml('?xml', array('version'=>'1.0', 'encoding'=>'utf-8'));
    $xml .= $this->build_xml('!DOCTYPE', array('svg', 'PUBLIC', '-//W3C//DTD SVG 1.1//EN', 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'));
    $xml .= $this->build_xml('svg', array('width'=>'' . $this->width, 'height'=>'' . $this->height, 'x'=>'0px', 'y'=>'0px', 'version'=>'1.1', 'xmlns'=>'http://www.w3.org/2000/svg', 'xmlns:xlink'=>'http://www.w3.org/1999/xlink', 'xml:space'=>'preserve', 'viewBox'=>'0 0 ' . $this->width . ' ' . $this->height));

    /*print("fields: <pre>");
    print_r($this->fields);
    print("</pre>");*/
    $xml .= $this->build_style_from_style($this->style);
    $xml .= $this->build_xml_from_fields($this->fields);

    $xml .= '</svg>';

    return $xml;
  }


  /**
   *
   */
  public function add_field($index, $name, $options, $style = '') {
    $this->build_field($index, $name, $options);
    if (is_array($style)) {
      $this->build_style('#' . $name, $style);
    }
  }


  /**
   *
   */
  public function add_style($name, $options) {
    $this->build_style($name, $options);
  }


  /**
   *
   */
  public function add_element($index, $name, $options, $content, $style='') {
    $this->build_field($index, $name, $options, $content);
    if (is_array($style)) {
      $this->build_style('#' . $name, $style);
    }
  }


  /**
   *
   */
  public function add_pattern($index, $name, $tag, $options, $style = '') {
    $this->build_pattern($index, $name, $tag, $options);
    if (is_array($style)) {
      $this->build_style('#' . $index . '.' . $options['class'], $style);
    }
  }


  /**
   * Pass in an index - it will appear as a tag
   * Note: using an underscore will make the index appear
   * as a tag with an id.
   * EX: 
   * $index == text
   * tag    == <text>
   * $index == text_style
   * tag    == <text id="style">
   * The code will only read one underscore.
   */
  public function add_child_tag($nested, $index, $name, $options, $content='', $style='') {
    $this->build_field($index, $name, $options, $content, $nested);
    if (is_array($style)) {
      $this->build_style('#' . $index . '.' . $options['class'], $style);
    }
  }


  /**
   *
   */
  public function remove_field($index) {
    unset($this->fields[$index]);
  }


  /* PROTECTED FUNCTIONS */
  /**
   *
   */
  protected function build_field($index, $name, $options, $content='', $nested='') {
    if ($nested) {
      if (!isset($this->fields->$nested)) {
        $this->fields->$nested = new \stdClass();
      }

      $options['id'] = $index;
      $this->fields->$nested->$index = array('tag' => $name, 'opts' => $options);
      if ($content) {
        $this->fields->$nested->$index['content'] = $content;
      }

    } else {
      $options['id']  = $name;
      $tag            = $options['tag'];
      unset($options['tag']);
      $this->fields->$index = array('tag' => $tag, 'opts' => $options);
      if ($content) {
        $this->fields->$index['content'] = $content;
      }
    }
  }

  
  /**
   *
   */
  protected function build_style($name, $options) {
    $this->style->$name = $options;
  }

  
  /**
   *
   */
  protected function build_pattern($index, $name, $tag, $options) {
    if (!isset($this->fields->defs)) {
      $this->fields->defs = array();
    }

    $this->fields->defs[$index]['tag']     = 'pattern';
    $this->fields->defs[$index]['id']      = $name;
    $this->fields->defs[$index]['opts']    = $options['parent'];
    $options['child']['id']                = $index;
    $this->fields->defs[$index]['content'] = array(array(
      'tag' => $tag,
      'opts' => $options['child'],
    ));
  }


  /**
   *
   */
  protected function build_xml($tag, $options = array(), $end = FALSE) {
    $xml = '<' . $tag;

    if(!empty($options)) {
      foreach ($options as $k=>$v) {
        if (is_int($k)) {
          $xml .= ' ' . $v;
        } else {
          $xml .= $this->generate_xml($k, $v);
        }
      }
    }

    $xml .= ($tag == '?xml') ? '?>' : '>';

    if ($end) {
      $xml = '</' . $tag . '>';
    }
    return $xml;
  }


  /**
   * 
   */
  protected function build_xml_from_fields($fields) {
    $xml = '';

    if (isset($fields->defs)) {
      $xml .= '<defs>';
      foreach($fields->defs as $index => $field) {
        $xml .= '<' . $field['tag'] . ' id="' . $field['id'] . '"';
        if (!empty($field['opts'])) {
          foreach ($field['opts'] as $k => $v) {
            if ($k == 'generate') {
              $parts = explode('_', $v);
              $parts = array_reverse($parts);
              $xml .= $this->$v($parts);
              continue;
            }
            $xml .= $this->generate_xml($k, $v);
          }
        }
        $xml .= '>';
        $xml .= $this->build_xml_from_fields($field['content']);
        $xml .= '</' . $field['tag'] . '>';
      }
      $xml .= '</defs>';

      $this->remove_field('defs');
    }

    foreach ($fields as $index => $field) {
      if (is_object($field)) {
        $index   = explode('_', $index);
        $xml    .= '<' . $index[1] . '>';
        $xml    .= $this->build_xml_from_fields($field);
        $xml    .= '</' . $index[1] . '>';

      } else if (is_array($field)) {
        $xml .= '<' . $field['tag'];
        if (!empty($field['opts'])) {
          foreach ($field['opts'] as $k => $v) {
            if ($k == 'generate') {
              $parts = explode('_', $v);
              $parts = array_reverse($parts);
              $xml .= $this->$v($parts);
              continue;
            }
            $xml .= $this->generate_xml($k, $v);
          }
        }
        $xml .= '>';
        if (array_key_exists('content', $field)) {
          if ($field['tag'] !== 'foreignobject') {
            $xml .= '<![CDATA[ ' . $field['content'] . ' ]]>';
          } else {
            $xml .= $field['content'];
          }
        }
        $xml .= '</' . $field['tag'] . '>';
      }
    }
    return $xml;
  }


  /**
   * 
   */
  protected function build_style_from_style($style) {
    $css  = '<style>/* <![CDATA[ */';
    
    foreach ($style as $name => $vals) {
      if (preg_match("/\@media/", $name)) {
        $css .= $name . '{';
        foreach ($vals as $id => $val) {
          $css .= $id . '{';
          foreach ($val as $k => $v) {
            $css .= $this->generate_css($k, $v);
          }
          $css .= '}';
        }
        $css .= '}';
      } else {
        $css .= $name . '{';
        foreach ($vals as $k => $v) {
          $css .= $this->generate_css($k, $v);
        }
        $css .= '}';
      }
    }

    $css .= '/* ]]> */</style>';
    return $css;
  }


  /* PRIVATE FUNCTIONS */
  /**
   *
   */
  private function generate_xml($name, $val) {
    return ' ' . $name . '="' . $val .'"';
  }

  
  /**
   *
   */
  private function generate_css($name, $val) {
    return $name . ':' . $val .';';
  }


  /**
   * specific to Office Depot coupon:
   * generate and place an arrow appropriately
   */
  private function _generate_od_arrow_points($parts) {
    $w = $this->width;
    $h = $this->height;
    $points = array();

    $points_x   = floor($this->width * 0.482);
    $points_y   = floor($this->height * 0.716);

    $points[] = $points_x . ',' . $points_y;
    $points[] = ($points_x + 20) . ',' . $points_y;
    $points[] = ($points_x + 10) . ',' . ($points_y + 15);

    return $this->generate_xml($parts[0], implode(' ', $points));
  }
}
